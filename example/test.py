from latexPlot import PubPlot, mmToInch
import matplotlib.pyplot as plt

# General plot settings, OPTIONAL
# Custom settings can be used for uniform plot dimensions and style
settings = {
    'width': mmToInch(120),
    'ratio': 1,
    'rcParams': {
        'legend.fancybox': 'false',
        'legend.edgecolor': 'inherit',
        'legend.loc': 'upper right',
        'text.usetex': True,
        'lines.linewidth': 1,
        'xtick.labelsize': 10,
        'ytick.labelsize': 10,
        'axes.labelsize': 10,
        'legend.fontsize': 10,
        'text.latex.preamble': r'\usepackage{nicefrac}'
    }
}

pp = PubPlot("Test plot", xlabel=r'\nicefrac{x}{m}', ylabel=r'\nicefrac{y}{m}', legend=True, settings=settings)

dataX = [1,2,4,5]
dataY = [2,10,30,50]

pp.plotAdd(plt.plot, dataX, dataY, label="Line plot")
pp.plotAdd(plt.scatter, dataX, dataY, label="Scatter plot")
pp.plotExport()