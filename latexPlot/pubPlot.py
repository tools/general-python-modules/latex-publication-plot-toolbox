import matplotlib
import palettable
import matplotlib.pyplot as plt
import numpy as np
import os
import json
from pathlib import Path
from cycler import cycler


def clamp(data, low, high, setLowLevel=None):
    clamped = np.copy(data)

    if not low >= 0 and low < 1 and low < high:
        raise ValueError(
            "Low limit not sensible. Should be between 0...1 and lower than high limit.")

    if not high >= 0 and high <= 1 and high > low:
        raise ValueError(
            "High limit not sensible. Should be between 0...1 and higher than low limit.")

    minimum = np.min(clamped)
    maximum = np.max(clamped)
    difference = maximum - minimum
    lowLevel = low*difference + minimum
    highLevel = maximum - (1-high)*difference

    if setLowLevel is None:
        clamped[clamped < lowLevel] = lowLevel
    else:
        clamped[clamped < lowLevel] = setLowLevel

    clamped[clamped > highLevel] = highLevel
    return clamped


def rebuildFontCache():
    """
    Rebuilds the global matplotlib font cache
    """
    matplotlib.font_manager._rebuild()


def loadPlotSettings(filename):
    """
    Loads matplotlib settings from a file
    """
    f = open(filename, "r")
    settings = json.loads(f.read())
    f.close()

    return settings


def mmToInch(mm):
    """
    Convert mm to inches, for matplotlib size settings
    """
    return mm/25.4


def mag10dB(val):
    """
    Convert linearly scaled value to 10*log10() representation
    Useful for power inputs
    """
    return 10*np.log10(np.abs(val))


def mag20dB(val):
    """
    Convert linearly scaled value to 20*log10() representation
    Useful for field inputs
    """
    return 20*np.log10(np.abs(val))


class PubPlot():
    """
    This class allows plotting of publication quality plots for Latex in png and pdf format

    Parameters:
    -----------
    name : str
        Name of the output file after export

    xlabel : str
        The label of the x-axis, optional

    ylabel : str
        The label of the y-axis, optional

    xlim : tuple of doubles
        Limit of the plot axis in x-direction

    ylim : tuple of doubles
        Limit of the plot axis in y-direction

    legend : bool
        Enables or disables legend display

    grid : bool
        Enables or disables grid display

    path : str
        The path for storage of the exported plot image

    settings : settings dictionary
        Controls plot appearance
    """

    def __init__(self, name, xticks=None, yticks=None, xlabel=None, ylabel=None, xlim=None, ylim=None, legend=False, grid=False, path=None, settings=None):

        if path is None:
            # Default path if not specified
            self.path = "./export"
        else:
            self.path = path

        self.plotFns = []

        self.name = name

        self.legend = legend
        self.grid = grid

        self.xticks = xticks
        self.yticks = yticks
        self.xlabel = xlabel
        self.ylabel = ylabel
        self.xlim = xlim
        self.ylim = ylim

        try:
            self.colorMap = settings["colorMap"]
        except:
            self.colorMap = palettable.colorbrewer.qualitative.Set1_9

        try:
            self.monochrome = settings["monochrome"]
        except:
            self.monochrome = False

        try:
            self.width = settings["width"]
        except:
            self.width = 3.487

        try:
            ratio = settings["ratio"]
            self.height = self.width / ratio
        except:
            self.height = self.width / 1.618

        # Latex export parameters that influence plot appearance
        paramsLocal = {
            'legend.loc': 'upper right',
            'text.usetex': True,
            'font.family': 'sans-serif',
            'font.serif': 'Times',
            'font.sans-serif': 'Open Sans, Helvetica',
            'xtick.labelsize': 8,
            'ytick.labelsize': 8,
            'axes.labelsize': 8,
            'legend.fontsize': 8,
            'text.latex.preamble': r'\usepackage{amsmath} \n \usepackage{amssymb} \n \usepackage{nicefrac}'
        }

        # Set selected parameters globally for matplotlib
        matplotlib.rcParams.update(paramsLocal)

        try:
            # Set selected parameters globally for matplotlib
            matplotlib.rcParams.update(settings["rcParams"])
        except:
            pass

    def __colorCycler(self):
        """
        Cylces through colors of the colormap for multiple plots
        """
        return cycler('color', self.colorMap.mpl_colors)

    def __plotExecute(self):
        """
        Performs the actual plotting of data
        """

        if not self.monochrome:
            cmap = plt.get_cmap("viridis")
            styles = self.__colorCycler()

        else:
            cmap = plt.get_cmap("gray")

            styles = (cycler('linestyle', ['-', ':', '--',  '-.']) * cycler(
                'color', [(0, 0, 0), (0.8, 0.8, 0.8), ]))
            # styles = (cycler('color', ['k']) * cycler('marker', ['', '.', '+', 'x']) * cycler('linestyle', ['-', ':', '--',  '-.'])
            #          )

        # Select the currently chosen line style from the cycler
        plt.rcParams['axes.prop_cycle'] = styles

        self.__plotInit()

        zo = 0
        for pltFnSet in self.plotFns:
            pltFn, plotArgs, plotKwargs = pltFnSet

            plt.sca(self.axis)

            # For contour plots, forcing rasterization is done differently
            if pltFn == plt.contourf or pltFn == plt.contour:
                pltFn(*plotArgs, **plotKwargs, cmap=cmap, zorder=-20)
                self.axis.set_rasterization_zorder(-10)

            # For all other plots, just enable rasterization
            else:
                pltFn(*plotArgs, rasterized=True, zorder=zo, **plotKwargs)
                zo -= 10

    def plotExport(self, store=True, overwrite=True):
        """
        Calls the plot execution function and then applies settings as chosen previously. Data storage also happens here.

        Parameters:
        -----------
        store : bool
            Enables or disables data storage to file
        """

        pngPath = '%s/png/%s.png' % (self.path, self.name)
        pdfPath = '%s/pdf/%s.pdf' % (self.path, self.name)

        pngFile = Path(pngPath)
        pdfFile = Path(pdfPath)

        # Check for existing plots
        if not (pngFile.is_file() or pdfFile.is_file()) or overwrite:
            self.__plotExecute()

            if self.xticks is not None:
                self.axis.set_xticks(self.xticks)

            if self.yticks is not None:
                self.axis.set_yticks(self.yticks)

            if self.ylabel is not None:
                self.axis.set_ylabel(self.ylabel)

            if self.xlabel is not None:
                self.axis.set_xlabel(self.xlabel)

            if self.xlim is not None:
                self.axis.set_xlim(self.xlim)

            if self.ylim is not None:
                self.axis.set_ylim(self.ylim)

            if self.grid:
                plt.grid(axis='y', color="0.9", linestyle='-', linewidth=1)

            if self.legend:
                plt.legend()

            self.fig.set_size_inches(self.width, self.height)
            plt.tight_layout()

            if store:
                while True:
                    try:
                        self.fig.savefig(pngPath, dpi=400)
                        self.fig.savefig(pdfPath, dpi=400)

                    except FileNotFoundError:
                        print("Generating directories...")
                        os.makedirs('%s/png' % (self.path))
                        os.makedirs('%s/pdf' % (self.path))

                    else:
                        break

    def __plotInit(self):
        """
        Initializes figure and axis handles for later plotting
        """
        self.fig, self.axis = plt.subplots(num=self.name)

    def plotAdd(self, plotFn, *plotArgs, **plotKwargs):
        """
        Add a plot to the list of plots for later execution

        Parameters:
        -----------
        plotFn : function
            The actual plotting function to be called later (e.g. "plot" or "surface")

        plotArgs : *
            Plotting arguments, including data to plot, depending on plotFn argument list

        plotKwargs : **
            Plotting keyword-arguments, depending on plotFn argument list
        """
        self.plotFns.append((plotFn, plotArgs, plotKwargs))

    def plotSingle(self, plotFn, *plotArgs, **plotKwargs):
        """
        Plots a single plot function and handles all initialization directly

        Parameters:
        -----------
        plotFn : function
            The actual plotting function to be called later (e.g. "plot" or "surface")

        plotArgs : *
            Plotting arguments, including data to plot, depending on plotFn argument list

        plotKwargs : **
            Plotting keyword-arguments, depending on plotFn argument list
        """
        self.plotAdd(plotFn, *plotArgs, **plotKwargs)
        self.plotExport()
